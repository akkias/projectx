@extends('layouts.marketing')
@section('content')
<section class="cover-wrapper relative">
	<div class="content text-center">
		<h1>Lorem Ipsum is simply dummy text</h1>
		<h2>Store, share and discover inspiring road trips</h2>
		<a href="#" class="btn btn-primary btn-lg">Get started</a>
		<a href="#" class="scroll-to-content absolute"><i class="ion-chevron-down"></i></a>
	</div>
</section>
<section class="stats">
	<div class="container text-center">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<h4>Total kilometers travelled</h4>
				<h2>11,111,111</h2>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<h4>Total rides completed</h4>
				<h2>11,111,111</h2>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<h4>Total Riders</h4>
				<h2>11,111,111</h2>
			</div>
		</div>
	</div>
</section>
<section class="featured-trips">
	<div class="container">
		<h2 class="section-title">Lorem Ipsum is simply</h2>
		<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 data-card">
				<img src="holder.js/100x100" class="data-img pull-left">
				<div class="content">
					<h4>Title</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 data-card">
				<img src="holder.js/100x100" class="data-img pull-left">
				<div class="content">
					<h4>Title</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 data-card">
				<img src="holder.js/100x100" class="data-img pull-left">
				<div class="content">
					<h4>Title</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 data-card">
				<img src="holder.js/100x100" class="data-img pull-left">
				<div class="content">
					<h4>Title</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="upcoming-rides">
	<div class="container">
		<h2 class="section-title">upcoming rides</h2>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="thumbnail ride-card">
				<div class="clearfix user-data">
					<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
					<a href="#">Akshay Sonawnae</a>
				</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
					<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
						  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
						    <span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
						<ul class="list-unstyled list-inline">
						    <li>4 stops</li>
						    <li>&middot;</li>
						    <li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="thumbnail ride-card">
				<div class="clearfix user-data">
					<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
					<a href="#">Akshay Sonawnae</a>
				</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
					<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
						  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
						    <span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
						<ul class="list-unstyled list-inline">
						    <li>4 stops</li>
						    <li>&middot;</li>
						    <li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="thumbnail ride-card">
				<div class="clearfix user-data">
					<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
					<a href="#">Akshay Sonawnae</a>
				</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
					<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
						  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
						    <span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
						<ul class="list-unstyled list-inline">
						    <li>4 stops</li>
						    <li>&middot;</li>
						    <li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="thumbnail ride-card">
				<div class="clearfix user-data">
					<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
					<a href="#">Akshay Sonawnae</a>
				</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
					<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
						  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
						    <span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
						<ul class="list-unstyled list-inline">
						    <li>4 stops</li>
						    <li>&middot;</li>
						    <li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="thumbnail ride-card">
				<div class="clearfix user-data">
					<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
					<a href="#">Akshay Sonawnae</a>
				</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
					<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
						  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
						    <span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
						<ul class="list-unstyled list-inline">
						    <li>4 stops</li>
						    <li>&middot;</li>
						    <li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="thumbnail ride-card">
				<div class="clearfix user-data">
					<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
					<a href="#">Akshay Sonawnae</a>
				</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
					<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
						  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
						    <span class="sr-only">80% Complete (danger)</span>
						  </div>
						</div>
						<ul class="list-unstyled list-inline">
						    <li>4 stops</li>
						    <li>&middot;</li>
						    <li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="col-xs-12 buttons text-center">
				<a href="/users/sign_up" class="btn btn-success">Sign up now</a>
				<a href="/explore/all" class="btn btn-default">Explore more rides</a>
			</div>
			
		</div>
	</div>
</section>
@endsection