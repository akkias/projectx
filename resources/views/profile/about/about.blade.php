@extends('layouts.master')
@section('content')
<div class="row">
	@include('profile.profile_lhs')
	<div class="col-md-9 profile-rhs">
		@include('profile.profile_subnav')
		<div class="content">
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 box">
				<div class="panel panel-default">
					<div class="panel-body text-center">
						<i style="font-size: 48px" class="ion-trophy"></i>
					</div>
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 box">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="ion-android-bicycle"></i> Current Motorcycle</h3>
					</div>
					<div class="panel-body">
						Royal Enfield Classic
					</div>
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 box">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="ion-android-bicycle"></i> Motorcycle Ridden</h3>
					</div>
					<div class="panel-body">
						Royal Enfield Classic
					</div>
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 box">
				<div class="panel panel-danger">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="ion-ios-medkit"></i> Medical Condition</h3>
					</div>
					<div class="panel-body">
						<ul class="list-unstyled med-cond">
						    <li>
						    	<strong>Blood Group:</strong> <span>O<sup>+</sup></span>
						    </li>
						    <li>
						    	<strong>Medication taken:</strong> <span>None</span>
						    </li>
						    <li>
						    	<strong>Known diseases:</strong> <code>Diabetes</code> <code>Blood Pressure</code>
						    </li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 box">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="ion-android-call"></i> Contact Details</h3>
					</div>
					<div class="panel-body">
						<ul class="list-unstyled med-cond">
						    <li>
						    	<strong>Primary:</strong> <span>+91 9158420989</span>
						    </li>
						    <li>
						    	<strong>Emergency:</strong> <span>+91 9158420989</span>
						    </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection