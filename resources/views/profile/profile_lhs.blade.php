<div class="col-md-3 profile-lhs">
		<div class="profile-info text-center">
		<a href="" class="edit"><i class="ion-gear-a"></i></a>
			<div class="avatar-holder img-circle">
				<img src="http://forum.extalia.net/image.php?u=33963&dateline=1354056266" alt="" width="125" class="img-circle">
			</div>
			<h4 class="toggleDrawer">Akshay Sonawane</h4>
			<p><i class="vmiddle glyphicon glyphicon-map-marker"></i> <small>Pune, India</small></p>
			<ul class="list-inline list-unstyled main-actions">
			    <li><button type="button" class="btn btn-primary">Connect</button></li>
			    <li><button type="button" class="btn btn-default">Follow</button></li>
			</ul>
			<ul class="list-inline list-unstyled list-social">
			    <li><a href="#" class="fb"><i class="ion-social-facebook"></i></a></li>
			    <li><a href="#" class="tw"><i class="ion-social-twitter"></i></a></li>
			    <li><a href="#" class="ig"><i class="ion-social-instagram-outline"></i></a></li>
			    <li><a href="#" class="gp"><i class="ion-social-googleplus-outline"></i></a></li>
			</ul>
		</div>
		<div class="profile-meta">
			<ul class="list-unstyled">
			    <li>
			    	<em class="img-circle text-center">s</em>
			    	<span>10 Rides Completed</span>
			    </li>
			    <li>
			    	<em class="img-circle text-center"><i class="ion-speedometer"></i></em>
			    	<span>12345 KMS Covered</span>
			    </li>
			    <li>
			    	<em class="img-circle text-center"><i class="ion-link"></i></em>
			    	<span>20 Connections</span>
			    </li>
			</ul>
		</div>
	</div>