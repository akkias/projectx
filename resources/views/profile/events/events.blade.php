@extends('layouts.master')
@section('content')
<div class="row">
	@include('profile.profile_lhs')
	<div class="col-md-9 profile-rhs">
	@include('profile.profile_subnav')
		<div class="content">
		<div class="table-responsive">
		<table class="table trip-table">
			<thead>
				<tr class="text-muted">
					<th width="32%">Event Name</th>
					<th class="text-center" width="8%">Days</th>
					<th class="text-center" width="8%">KMs</th>
					<th class="text-center" width="12%">Stops</th>
					<th class="text-center" width="12%">Going/invited</th>
					<th class="text-center" width="14%">Rated by biking clubs</th>
					<th class="text-center" width="14%">Rated by bikers</th>
				</tr>
			</thead>
			<tbody>
				@include('profile.trip_row')
				@include('profile.trip_row')
				@include('profile.trip_row')
				@include('profile.trip_row')
				@include('profile.trip_row')
				@include('profile.trip_row')
				@include('profile.trip_row')
				@include('profile.trip_row')
			</tbody>
		</table>
		</div>
			
		</div>
	</div>
</div>
@endsection