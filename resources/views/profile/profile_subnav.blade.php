<div class="subnav">
	<ul class="nav nav-pills">
	  <li role="presentation" class="{{ Request::is('username') ? 'active' : '' }}"><a href="/username">About Me</a></li>
	  <li class="{{ Request::is('username/events') ? 'active' : '' }}"><a href="/username/events">Toured Places</a></li>
	  <li class="{{ Request::is('username/connections') ? 'active' : '' }}"><a href="#">Dashboard</a></li>
	  <li class="{{ Request::is('username/timeline') ? 'active' : '' }}"><a href="/username/timeline">Activity log / Timeline</a></li>
	</ul>	
</div>