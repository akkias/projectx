<article class="col-md-4 col-sm-6 col-xs-12 trip-card animated slideInUp duration-010s">
	<figure>&nbsp;<span class="kms">1020 km &middot; 10 stops</span></figure>
	<div class="card-content">
		<h4><a href="#">Glaciers, waterfalls, hikes and more: These are Alaska's best drives</a></h4>
	</div>
</article>