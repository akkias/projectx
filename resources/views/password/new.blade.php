@extends('layouts.marketing')
@section('content')
<section class="login-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="row">
					<div class="login-box">
						<div class="with-email col-md-12">
							<h3 class="text-center">Forgot your password?</h3>
							<p class="text-muted">Enter your email address and we will send you the instructions to reset your password.</p>
							<br>
							<form action="" method="POST" role="form">
								<div class="form-group">
									<input type="email" class="form-control" id="" placeholder="Email id" required>
								</div>
								<a class="back-to pull-right" href="/users/sign_in"><i class="ion-ios-arrow-left"></i> Back to login</a>
								<button type="submit" class="btn btn-primary btn-action">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection