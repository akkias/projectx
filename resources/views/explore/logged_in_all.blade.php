@extends('layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="explore logged-in">
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="thumbnail ride-card">
					<div class="clearfix user-data">
						<small class="text-muted pull-right">Created 1 hour ago</small>
						<img src="https://res.cloudinary.com/hnz6dacyv/image/upload/w_40,h_40,c_fill/profilePics/1602/10985566_1037820199566573_63600427230314899_n" class="img-circle">
						<a href="#">Akshay Sonawnae</a>
					</div>
					<div class="ride-cover" style="background: url(https://images.unsplash.com/photo-1414500923875-1704944d8df7?q=80&fm=jpg&w=1080&fit=max&s=f301c7c776810bd0d8018af3caa273d0) center /  cover;"></div>
					<div class="caption">
						<h3 class="name"><a href="#">Ride name</a></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</p>
						<p>April 1, 2015</p>
						<div class="progress">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
								<span class="sr-only">80% Complete (danger)</span>
							</div>
						</div>
						<ul class="list-unstyled list-inline">
							<li>4 stops</li>
							<li>&middot;</li>
							<li>1000 KMs</li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection