<head>
    <title>ProjectX</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" type="text/css" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/bower_components/animate-css/animate.min.css">
	<link rel="stylesheet" type="text/css" href="/bower_components/ionicons/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/app.css">
</head>