<footer class="footer">
	<div class="container">
		<div class="row">	
			<div class="col-xs-12 text-center">
				&copy; 2015
			</div>
		</div>
	</div>
</footer>
<!-- Latest compiled and minified JS -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/holderjs/holder.js"></script>
<script src="/assets/lib/app.js"></script>