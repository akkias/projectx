<!DOCTYPE html>
<html>
@include('shared.head')
<body class="fadeIn animated has-drawer">
    @include('shared.header')
    <div class="container-fluid">
        <div class="content">
            @yield('content')
            @include('shared.footer')
        </div>
    </div>
</body>
</html>