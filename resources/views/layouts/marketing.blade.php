<!DOCTYPE html>
<html>
@include('shared.head')
<body class="fadeIn animated has-drawer">
	@include('shared.header_marketing')
	@yield('content')
	@include('shared.footer')
</body>
</html>