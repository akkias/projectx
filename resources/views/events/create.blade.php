@extends('layouts.master')
@section('content')
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
	function initialize() {
		var mapCanvas = document.getElementById('map');
		var mapOptions = {
			center: new google.maps.LatLng(44.5403, -78.5463),
			zoom: 8,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		var map = new google.maps.Map(mapCanvas, mapOptions)
	}
	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div class="row">
	<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 map-wrapper">
		<div id="map"></div>
	</div> 
	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" id="createDrawer">
		<h3 class="text-center">Plan a ride</h3>
		<div class="content">
			<form action="" method="POST" role="form">
				<div class="btn-group form-group btn-group-justified" data-toggle="buttons">
					<label class="btn btn-default active">
						<input type="radio" name="options" id="option1" autocomplete="off" checked> <i class="ion-android-person"></i>Individual
					</label>
					<label class="btn btn-default">
						<input type="radio" name="options" id="option2" autocomplete="off"> <i class="ion-android-people"></i>Group
					</label>
				</div>

				<div class="form-group">
					<label class="title">Name your trip</label>
					<input type="text" class="form-control" id="" placeholder="Input field">
				</div>
				<div class="row">
					<div class="form-group col-md-6 relative icon">
						<label class="title">Start point</label>
						<input type="text" class="form-control col-md-6" id="" placeholder="Input field">
						<i class="ion-android-pin"></i>
					</div>
					<div class="form-group col-md-6 relative icon">
						<label class="title">End point</label>
						<input type="text" class="form-control col-md-6" id="" placeholder="Input field">
						<i class="ion-android-pin"></i>
					</div>
				</div>
				<div class="btn-group form-group btn-group-justified avail-routes" data-toggle="buttons">
					<label class="btn btn-default active">
						<input type="radio" name="route-options" id="option1" autocomplete="off" checked> <i class="ion-android-checkmark-circle"></i>Route 1
					</label>
					<label class="btn btn-default">
						<input type="radio" name="route-options" id="option2" autocomplete="off"> <i class="ion-android-checkmark-circle"></i>Route 2
					</label>
					<label class="btn btn-default">
						<input type="radio" name="route-options" id="option2" autocomplete="off"> <i class="ion-android-checkmark-circle"></i>Route 3
					</label>
				</div>
				<div class="row">
					<div class="form-group col-md-6 relative icon">
						<label class="title">Start time</label>
						<input type="text" class="form-control col-md-6" id="" placeholder="Input field">
						<i class="ion-android-calendar"></i>
					</div>
					<div class="form-group col-md-6 relative icon">
						<label class="title">End time</label>
						<input type="text" class="form-control col-md-6" id="" placeholder="Input field">
						<i class="ion-android-calendar"></i>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-md-6 relative icon">
						<label class="title">Avg. kms/day</label>
						<input type="text" class="form-control col-md-6" id="" placeholder="Input field">
						<i class="ion-ios-speedometer-outline"></i>
					</div>
					<div class="form-group col-md-6 relative icon">
						<label class="title">Stops</label>
						<input type="text" class="form-control col-md-6" id="" placeholder="Input field">
						<i class="ion-android-hand"></i>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12 relative icon">
						<label class="title">Enter a friend's email address to share the trip and invite them to edit with you.</label>
						<input type="text" class="form-control col-md-6" id="" placeholder="Input field">
						<i class="ion-android-person-add"></i>
					</div>
				</div>
				<div class="table-responsive">


					<table class="table">
						<thead>
							<tr>
								<th width="70%">Name</th>
								<th width="25%">Status</th>
								<th width="5%">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Akshay Sonawane</td>
								<td><span class="text-muted">Invited</span></td>
								<td><a href="#" class="text-danger"><i href="" class="ion-android-cancel"></i></a></td>
							</tr>
							<tr>
								<td>Akshay Sonawane</td>
								<td><span class="text-muted">Invited</span></td>
								<td><a href="#" class="text-danger"><i href="" class="ion-android-cancel"></i></a></td>
							</tr>
							<tr>
								<td>Akshay Sonawane</td>
								<td><span class="text-muted">Invited</span></td>
								<td><a href="#" class="text-danger"><i href="" class="ion-android-cancel"></i></a></td>
							</tr>
							<tr>
								<td>Akshay Sonawane</td>
								<td><span class="text-muted">Invited</span></td>
								<td><a href="#" class="text-danger"><i href="" class="ion-android-cancel"></i></a></td>
							</tr>
							<tr>
								<td>Akshay Sonawane</td>
								<td><span class="text-muted">Invited</span></td>
								<td><a href="#" class="text-danger"><i href="" class="ion-android-cancel"></i></a></td>
							</tr>
							<tr>
								<td>Akshay Sonawane</td>
								<td><span class="text-muted">Invited</span></td>
								<td><a href="#" class="text-danger"><i href="" class="ion-android-cancel"></i></a></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 footer text-center">
					<button class="btn btn-primary" type="submit">Save</button>
				</div>
			</form>
		</div>
	</div> 
</div>
@endsection