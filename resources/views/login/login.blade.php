@extends('layouts.marketing')
@section('content')
<section class="login-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="row">
					<div class="login-box">
						<div class="with-email col-md-6">
							<h3 class="text-center">Login with email</h3>
							<form action="" method="POST" role="form">
								<div class="form-group">
									<input type="text" class="form-control" id="" placeholder="Username" title="At least 2 characters long. No special characters (including spaces) permitted." data-toggle="tooltip" data-placement="right" required>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" id="" placeholder="Password" required>
								</div>
								<button type="submit" class="btn btn-primary btn-block btn-action">Sign in</button>
								<p>
									<a href="/users/password/new">Forgot your password?</a><br>
									<a href="/users/confirmation/new">Didn't receive confirmation instructions?</a>
								</p>
							</form>
						</div>
						<div class="with-social col-md-6 login">
							<h3 class="text-center">Login with social media</h3>
							<div class="social-btns">
								<a class="btn-block btn-facebook btn" href=""><i class="ion-social-facebook"></i>Facebook</a>
								<a class="btn-block btn-twitter btn" href=""><i class="ion-social-twitter"></i>Twitter</a>
								<a class="btn-block btn-gplus btn" href=""><i class="ion-social-googleplus"></i>Google Plus</a>
							</div>
						</div>
					</div>
					<div class="col-md-12 text-center">
						Don't have an account yet? <a href="/users/sign_up">Sign up</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection