<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home.home');
});
Route::get('users/sign_in', function () {
    return view('login.login');
});
Route::get('users/sign_up', function () {
    return view('signup.signup');
});
Route::get('users/password/new', function () {
    return view('password.new');
});
Route::get('users/confirmation/new', function () {
    return view('confirmation.new');
});
Route::get('explore/all', function () {
    return view('explore.explore');
});
Route::get('explore/all_logged', function () {
    return view('explore.logged_in_all');
});
Route::get('explore/my', function () {
    return view('explore.my');
});
Route::get('username/timeline', function () {
    return view('profile.timeline.timeline');
});
Route::get('search', function () {
    return "Search";
});
Route::get('clubs', function () {
    return "All Clubs";
});
Route::get('events', function () {
    return "All Events";
});
Route::get('members', function () {
    return "Members";
});
Route::get('events/create', function () {
    return view('events.create');
});
Route::get('username', function () {
    return view('profile.about.about');
});
Route::get('username/clubs', function () {
    return "Akshay's Clubs";
});
Route::get('username/events', function () {
    return view('profile.events.events');
});