var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    livereload = require('gulp-livereload'),

    jshint     = require('gulp-jshint'),
    sass       = require('gulp-ruby-sass'),
    concat     = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps');

    input  = {
      'sass': 'resources/assets/sass/**/*.scss',
      'javascript': 'resources/assets/js/**/*.js'
      //'vendorjs': 'public/assets/javascript/vendor/**/*.js'
    },

    output = {
      'stylesheets': 'public/assets/css/**/*.css',
      'javascript': 'public/assets/js/**/*.js'
    };

/* run javascript through jshint */
gulp.task('jshint', function() {
  return gulp.src(input.javascript)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

/* compile scss files */
gulp.task('sass', function() {
  //return gulp.src('')
  return sass('resources/assets/sass', { style: 'compressed', sourcemap: true })
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/assets/css'))
    .pipe(livereload());
    //.pipe(gulp.dest(output.stylesheets));
});

/* concat javascript files, minify if --type production */
gulp.task('build-js', function() {
  return gulp.src(input.javascript)
    .pipe(sourcemaps.init())
      .pipe(concat('bundle.js'))
      //only uglify if gulp is ran with '--type production'
      .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop()) 
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(output.javascript));
});

/* Watch these files for changes and run the task on update */
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch(input.javascript, ['jshint', 'build-js']);
  gulp.watch(input.sass, ['sass']);
});

// create a default task and just log a message
gulp.task('default', function() {
  return gutil.log('Gulp is running!')
});

/* run the watch task when gulp is called without arguments */
gulp.task('default', ['watch']);